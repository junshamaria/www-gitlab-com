---
layout: handbook-page-toc
title: GitLab Partner Bootcamp
---

# GitLab Partner Bootcamp

GitLab Partner Bootcamp was designed in 2020 to help you get up to speed on implementing GitLab for your customers, by providing self-paced Training and PSE Certification, where you can learn about and practice most common use-cases and scenarios.

As of **4th November 2021**, the Bootcamp has been deprecated in favour of a new Training Curriculum and Certification process - please refer to [this page](https://about.gitlab.com/services/pse-certifications/pse-specialist/) for updated details.
As of the above date, the original Bootcamp Lab Exercises and Certification elements have been removed;  the educational videos and and associated material remain available as a learning resource, **but will themselves be deprecated and removed in May 22**.

Bootcamp material is divided into sections:

1. Introduction to GitLab
1. Using GitLab
1. Implementing GitLab

These sections are independent, so you can take any of them in any order depending on your needs; we recommend starting with the "Using GitLab" and moving on to "Implementing GitLab"

## Introduction to GitLab

GitLab is a single application for the entire DevSecOps lifecycle. To see what GitLab does have a look at this demo video.
<figure class="video_container"><iframe src="https://www.youtube.com/embed/-_CDU6NFw7U" height="315" width="560"></iframe></figure>

### GitLab Basics for Developers

If you're brand new to GitLab, we recommend watching this video to get familiar with the basics of the development workflow
<figure class="video_container"><iframe src="https://www.youtube.com/embed/7J7Fdh6XSvU" height="315" width="560"></iframe></figure>

## Using GitLab

### Agile Project Management

<figure class="video_container"><iframe src="https://www.youtube.com/embed/bCDmxG4IIXA" height="315" width="560"></iframe></figure><br>
If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage, review [additional material related to this usecase](https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/agile/).

### Code Management & Version Control

<figure class="video_container"><iframe src="https://www.youtube.com/embed/L6YAUSV51-Y" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/13MmRlHat_-gRntPijyG9uu7nn1fzfCe3/view?usp=sharing)

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/version-control-collaboration/">additional material related to this usecase</a>.

### Continuous Integration & AutoDevOps

<figure class="video_container"><iframe src="https://www.youtube.com/embed/q382wEA4wv0" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/1rGiDua7LXNTR0sx5kpyK7tCRe9eJZp9O/view?usp=sharing).
If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/ci/">additional material related to this usecase</a>.

### Custom CI Pipelines

<figure class="video_container"><iframe src="https://www.youtube.com/embed/nVzNFXLucWY" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/1PELf3Kshj_kfjagnGDl1lYLh3AuMn29Y/view?usp=sharing).

### GitOps

<figure class="video_container"><iframe src="https://www.youtube.com/embed/FZVW0w8j6BI" height="315" width="560"></iframe></figure>[Presentation Slide Deck](<a href="https://drive.google.com/file/d/1jxbK-wCKmfX1fIIcvy2hY3ycr18-BHGT/view?usp=sharing">https://drive.google.com/file/d/1jxbK-wCKmfX1fIIcvy2hY3ycr18-BHGT/view?usp=sharing</a>).

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/gitops/">additional material related to this usecase</a>.

### DevSecOps

<figure class="video_container"><iframe src="https://www.youtube.com/embed/cTzt1FqIX40" height="315" width="560"></iframe></figure>[Presentation Slide Deck](<a href="https://drive.google.com/file/d/1Q8axZ7774CZ0RDLLQ_UuAmVWiqDnWd1t/view?usp=sharing">https://drive.google.com/file/d/1Q8axZ7774CZ0RDLLQ_UuAmVWiqDnWd1t/view?usp=sharing</a>).

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/devsecops/">additional material related to this usecase</a>.

### Value Stream Management

<figure class="video_container"><iframe src="https://www.youtube.com/embed/Rltxmm3hgMk" height="315" width="560"></iframe></figure>[Presentation Slide Deck](<a href="https://drive.google.com/file/d/1ZQ5Q0KOQ6pJHEN5_7V3BTOPc61ihkM8A/view?usp=sharing">https://drive.google.com/file/d/1ZQ5Q0KOQ6pJHEN5_7V3BTOPc61ihkM8A/view?usp=sharing</a>).

If you are interested in market positioning, typical pain points and the way GitLab addresses them, customer stories and analyst coverage review <a href="https://about.gitlab.com/handbook/marketing/strategic-marketing/usecase-gtm/simplify-devops/">additional material related to this usecase</a>.

## Implementing GitLab

### Installation and Architecture

<figure class="video_container"><iframe src="https://www.youtube.com/embed/rsrjRLZxNd8" height="315" width="560"></iframe></figure>[Presentation Slide Deck](<a href="https://drive.google.com/file/d/1z6MmLqhgzFEXECi8_n7Xj9BTVPafkcdo/view?usp=sharing">https://drive.google.com/file/d/1z6MmLqhgzFEXECi8_n7Xj9BTVPafkcdo/view?usp=sharing</a>)
.
### Extending and Integrating

<figure class="video_container"><iframe src="https://www.youtube.com/embed/VjB7JE0a56o" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/1wx64uec4UXkSM9xAPMsD3hEVaEqQKMy4/view?usp=sharing).

### Continuous Delivery with Kubernetes

<figure class="video_container"><iframe src="https://www.youtube.com/embed/RkVRD0z7eT0" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/1LMrLs-Fi_FhVAQTqaGStr_Mg1UfhsKVK/view?usp=sharing).

### Runner Configurations

<figure class="video_container"><iframe src="https://www.youtube.com/embed/uXw-cAAUHrE" height="315" width="560"></iframe></figure>[Presentation Slide Deck](https://drive.google.com/file/d/1DShnAltAkph_z3BQgO8kgWCRBnElEgKk/view?usp=sharing).