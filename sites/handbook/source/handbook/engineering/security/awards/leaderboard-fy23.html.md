---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 900 |
| [@kassio](https://gitlab.com/kassio) | 2 | 600 |
| [@brodock](https://gitlab.com/brodock) | 3 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 600 |
| [@leipert](https://gitlab.com/leipert) | 5 | 500 |
| [@.luke](https://gitlab.com/.luke) | 6 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 7 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 8 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 9 | 500 |
| [@garyh](https://gitlab.com/garyh) | 10 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 11 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 12 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 13 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 14 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 15 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 16 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 17 | 150 |
| [@10io](https://gitlab.com/10io) | 18 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 19 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 20 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 22 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 23 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 24 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 25 | 60 |
| [@minac](https://gitlab.com/minac) | 26 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 27 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 28 | 60 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 29 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 30 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 31 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 32 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 33 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 34 | 30 |
| [@subashis](https://gitlab.com/subashis) | 35 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 36 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 37 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 38 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 39 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 40 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 41 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |
| [@zined](https://gitlab.com/zined) | 3 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vitallium](https://gitlab.com/vitallium) | 1 | 900 |
| [@kassio](https://gitlab.com/kassio) | 2 | 600 |
| [@brodock](https://gitlab.com/brodock) | 3 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 4 | 600 |
| [@leipert](https://gitlab.com/leipert) | 5 | 500 |
| [@.luke](https://gitlab.com/.luke) | 6 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 7 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 8 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 9 | 500 |
| [@garyh](https://gitlab.com/garyh) | 10 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 11 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 12 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 13 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 14 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 15 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 16 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 17 | 150 |
| [@10io](https://gitlab.com/10io) | 18 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 19 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 20 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 21 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 22 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 23 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 24 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 25 | 60 |
| [@minac](https://gitlab.com/minac) | 26 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 27 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 28 | 60 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 29 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 30 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 31 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 32 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 33 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 34 | 30 |
| [@subashis](https://gitlab.com/subashis) | 35 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 36 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 37 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 38 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 39 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 40 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 41 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@spirosoik](https://gitlab.com/spirosoik) | 1 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 2 | 200 |
| [@zined](https://gitlab.com/zined) | 3 | 200 |


